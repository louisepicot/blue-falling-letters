
Physijs.scripts.worker = 'js/librairies/physijs_worker.js'
Physijs.scripts.ammo = '../librairies/ammo.js';

let scene = new Physijs.Scene(),
    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer() : new THREE.CanvasRenderer(),
    light = new THREE.AmbientLight(0xffffff),
    camera,
    ground,
    wallRight,
    wallLeft,
    wallBack,
    controls,
    letterMaterial,
    arrayType = [],
    counter = 0,
    loader = new THREE.FontLoader();

const init = () => {
    scene.setGravity(new THREE.Vector3(0, -20, -20))
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0xffffff, 1);
    document.getElementById('three-container').appendChild(renderer.domElement);
    scene.add(light)

    camera = new THREE.PerspectiveCamera(35, window.innerWidth / window.innerHeight, 1, 1000)
    camera.position.set(0, 190, 540);
    camera.lookAt(scene.position)
    scene.add(camera)



    ///WALLL

    const lightDir = new THREE.DirectionalLight(0xffefcc);
    lightDir.position.set(0, 5, 150);
    lightDir.rotation.y = 10
    lightDir.name = 'lightDir'
    scene.add(lightDir)

    const lightDirBack = new THREE.DirectionalLight(0xffefcc);
    lightDirBack.position.set(0, 105, 100);
    lightDirBack.castShadow = true;
    lightDirBack.name = 'lightDirB'
    scene.add(lightDirBack)

    const lightDirRight = new THREE.DirectionalLight(0xffefcc);
    lightDirRight.position.set(0, 205, 50);
    lightDirRight.castShadow = true;
    lightDirRight.name = 'lightDirR'
    scene.add(lightDirRight)

    const lightDirLeft = new THREE.DirectionalLight(0xff01dd);
    lightDirLeft.position.set(30, -25, 0);
    lightDirLeft.castShadow = true;
    lightDirLeft.name = 'lightDirL'
    scene.add(lightDirLeft)

    let wallMaterial = Physijs.createMaterial(
        new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture('./assets/images/dessins-small/alice45.jpg'),
            shininess: 0,
            flatShading: THREE.FlatShading,
            side: THREE.DoubleSide
        }),
        0,
        0.6
    )
    wallMaterial.transparent = true;
    wallMaterial.opacity = 0.5;

    wallRight = new Physijs.BoxMesh(
        new THREE.CubeGeometry(200, 1, 550),
        wallMaterial,
        0
    );

    wallRight.name = 'wallRight';
    wallRight.rotation.x = 0;
    wallRight.rotation.y = 0;
    wallRight.rotation.z = 1.6;
    wallRight.position.y = 70;
    wallRight.position.x = 100;
    wallRight.position.z = 100;
    scene.add(wallRight)



    wallLeft = new Physijs.BoxMesh(
        new THREE.CubeGeometry(200, 1, 550),
        wallMaterial,
        0
    );

    wallLeft.name = 'wallLeft';
    wallLeft.rotation.x = 0;
    wallLeft.rotation.y = 0;
    wallLeft.rotation.z = 1.55;
    wallLeft.position.y = 70;
    wallLeft.position.x = -100;
    wallLeft.position.z = 100;

    scene.add(wallLeft)



    wallBack = new Physijs.BoxMesh(
        new THREE.CubeGeometry(200, 1, 200),
        wallMaterial,
        0
    );

    wallBack.name = 'wallBack';
    wallBack.rotation.x = 0;
    wallBack.rotation.y = 1.55;
    wallBack.rotation.z = 1.55;
    wallBack.position.y = 70;
    wallBack.position.x = 0;
    wallBack.position.z = 0;
    scene.add(wallBack)




    ground = new Physijs.BoxMesh(
        new THREE.CubeGeometry(500, 1, 550),
        wallMaterial,
        0
    );

    ground.name = 'ground';
    ground.position.y = 0;
    ground.position.z = 100;
    scene.add(ground)

    letterMaterial = Physijs.createMaterial(
        new THREE.MeshPhongMaterial({
            // color: 0x0088aa,
            // color: 0xff001d,
            color: 0x0000FF,
            specular: 0xffefcc,
            shininess: 100,
            flatShading: THREE.FlatShading,
            side: THREE.DoubleSide
        }),
        0,
        0.8
    )



    ground.addEventListener('collision', function (
        otherObject,
        relativeVelocity,
        relativeRotation,
        contactNormal) {

        if (otherObject.name == "ground") {
            // alert("hit the ground")
            console.log("otherObject", otherObject)
        }
    }
    )

    controls = new THREE.FlyControls(camera);
    controls.movementSpeed = 0.005;
    controls.domElement = document;
    controls.rollSpeed = 0.001;
    controls.autoForward = false;
    controls.maxZoom = 100



    loadFont()
    render();

}

const render = () => {

    scene.simulate();
    var lightDir = scene.getObjectByName("lightDir");
    var lightDirB = scene.getObjectByName("lightDirB");
    var lightDirR = scene.getObjectByName("lightDirR");
    var lightDirL = scene.getObjectByName("lightDirL");

    lightDir.rotation.x += 0.5
    lightDirB.rotation.x += 0.5
    lightDirR.rotation.x += 0.5
    lightDirL.rotation.x += 0.5
    controls.update(1);
    renderer.render(scene, camera);
    requestAnimationFrame(render);

}


function loadFont() {
    var loader = new THREE.FontLoader();
    loader.load('./Womb_Script_Regular.json', function (res) {
        font = res;
        createText();
    });
}
function createText() {
    var textgeo = new THREE.TextGeometry('Salut', {
        font: font,
        size: 35,
        height: 10,
        curveSegments: 1,
        weight: "regular",
        bevelThickness: 0.1,
        bevelSize: 0.1,
        bevelEnabled: true
    })

    textgeo.computeBoundingBox();
    textgeo.computeVertexNormals();



    for (let i = 0; i < 30; i++) {
        var text = new Physijs.BoxMesh(textgeo, letterMaterial, 1)
        text.position.z = Math.floor(Math.random() * 250) + 60
        text.position.x = Math.floor(Math.random() * -100) + 20
        text.position.y = Math.floor(Math.random() * -50) + 200
        // text.position.x = -textgeo.boundingBox.max.x / 2;
        text.castShadow = true;
        arrayType.push(text)
    }
}




window.onload = init();



window.setInterval(() => {
    console.log("ye", Math.floor(Math.random() * 120) + 60)
    scene.add(arrayType[counter])
    counter += 1
}, 7000)

//    setTimeout(function () {
//        document.location.reload(true)
//    }, 50000)