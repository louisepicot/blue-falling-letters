
// var canvas;
// const scenes = [],
//     yesNo = true,
//     containers = document.querySelectorAll(".three-container"),
//     loader = new THREE.OBJLoader();




var container;

var camera, scene, renderer;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
let controls;

init();
animate();


function init() {

    renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 1000);
    camera.position.z = 250;

    // scene

    scene = new THREE.Scene();

    const color = 0xFFFFFF;
    const intensity = 1;
    const light = new THREE.DirectionalLight(color, intensity);
    light.position.set(0, 10, 0);
    light.target.position.set(-5, 0, 0);
    scene.add(light);
    scene.add(light.target);




    const objLoader = new THREE.OBJLoader2();
    objLoader.loadMtl('../assets/ttest.mtl', null, (materials) => {
        objLoader.setMaterials(materials);
        objLoader.load('../assets/ttest.obj', (event) => {
            const root = event.detail.loaderRootNode;
            scene.add(root);
        });
    });


    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.getElementById('three-container').appendChild(renderer.domElement);


    //
    controls = new THREE.OrbitControls(camera)
    // controls.dispose();
    // controls.update();
    // document.addEventListener('mousemove', onDocumentMouseMove, false);


    window.addEventListener('resize', onWindowResize, false);

}

function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function onDocumentMouseMove(event) {
    controls.handleMouseMoveRotate(event)
    // mouseX = (event.clientX - windowHalfX) / 2;
    // mouseY = (event.clientY - windowHalfY) / 2;

}

//

function animate() {

    requestAnimationFrame(animate);
    render();

}

function render() {

    // camera.position.x += (mouseX - camera.position.x) * .05;
    // camera.position.y += (- mouseY - camera.position.y) * .05;
    controls.update();
    camera.lookAt(scene.position);

    renderer.render(scene, camera);

}





// window.onload = init()
// animate()


// function init() {


//     for (let i = 0; i < 1; i++) {

//         console.log(containers[i])
//         let scene = new THREE.Scene(),
//             renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
//             light = new THREE.AmbientLight(0xffffff),
//             camera,
//             controls;
//         containers[i].appendChild(renderer.domElement);

//         scene.userData.renderer = renderer
//         scene.add(light);

//         // const lightDir = new THREE.DirectionalLight(0x00ffc1);
//         // lightDir.position.set(0, 5, 100);
//         // lightDir.castShadow = true;
//         // scene.add(lightDir)
//         // const lightDirBack = new THREE.DirectionalLight(0x2500ff);
//         // lightDirBack.position.set(0, 5, -200);
//         // lightDirBack.castShadow = true;
//         // scene.add(lightDirBack)

//         // const lightDirRight = new THREE.DirectionalLight(0xc3b5ff);
//         // lightDirRight.position.set(-50, 25, 0);
//         // lightDirRight.castShadow = true;
//         // scene.add(lightDirRight)

//         // const lightDirLeft = new THREE.DirectionalLight(0x2500ff);
//         // lightDirLeft.position.set(50, -25, 0);
//         // lightDirLeft.castShadow = true;
//         // scene.add(lightDirLeft)


//         camera = new THREE.PerspectiveCamera(30, 1200 / 1200, 1, 1000);
//         camera.position.set(0, 0, 20);
//         camera.lookAt(scene.position)
//         scene.userData.camera = camera
//         scene.add(camera)


//         controls = new THREE.OrbitControls(camera)
//         controls.dispose();
//         controls.update();
//         scene.userData.controls = controls
//         containers[i].addEventListener('mousemove', (event) => { onDocumentMouseMove(event, controls) }, false);

//         // load a resource
//         loader.load(
//             // resource URL
//             '../assets/images/dessins-small/hypnose-3D.obj',
//             // called when resource is loaded
//             function (object) {

//                 scene.add(object);
//                 scene.userData.object = object

//             },
//             // called when loading is in progresses
//             function (xhr) {

//                 console.log((xhr.loaded / xhr.total * 100) + '% loaded');

//             },
//             // called when loading has errors
//             function (error) {

//                 console.log('An error happened');

//             }
//         );


//         renderer.setSize(1200, 1200);
//         renderer.setClearColor(0x000000, 1);
//         scenes.push(scene);
//     }


// }



// function onDocumentMouseMove(event, controls) {
//     controls.handleMouseMoveRotate(event)
//     // controls.handleMouseMoveRotate(event);

// }

// function animate() {
//     render()
//     requestAnimationFrame(animate);
// }


// function render() {
//     scenes.forEach(scene => {
//         scene.userData.controls.update();
//         let camera = scene.userData.camera
//         scene.userData.renderer.render(scene, camera)
//     })
// }





// window.onload = init();