
const text = document.getElementById("text");
const string = `I haw suggested that whiteness is a social and bodily orientation that extends what is within reach.
Fanon, as noted earlier, talks about the "white world" and how it fele s to inhabit a white world with a black body. We might say, 
then, that the world extends the form of some bodies more than others, and such bodies in turn feel at home in this world. 
We can now consider how whiteness is worldly, by rethinkingthe intimacy between habits and space. We might be used to thinking 
of bodies as "having" habits, usually bad ones. We could
even describe whiteness as a bad habit: as a series of actions that are repeated, forgotten, and that allow some
bodies to take up space by restricting the mobility of others. I want to explore here how public spaces take
shape through the habitual actions of bodies, such that the contours of space could be described as habitual. I
turn to the concept of habits to theorize not so much how bodies acquire their shape, but how spaces acquire the
shape ofthe bodies that "inhabit" them. We could think about the "habit" in the "inhabit."`


string.split("").forEach(word => {
    if (word !== " ") {
        word.split("").forEach(letter => {
            text.innerHTML += `<span class=\"ouf\">${letter}</span>`
        })
    } else {
        text.innerHTML += `<span class=\"ef\">${word}</span>`
    }

})



const spans = document.querySelectorAll('span')
let pointerBox,
    centerPoint,
    centers,
    centerX,
    centerY,
    radians,
    degree;




document.addEventListener('mousemove', (e) => {
    event.preventDefault
    spans.forEach((span, i) => {

        pointerBox = span.getBoundingClientRect()

        centerPoint = window.getComputedStyle(span).transformOrigin

        centers = centerPoint.split(" ")

        centerY = pointerBox.top + parseInt(centers[1]) - window.pageYOffset
        centerX = pointerBox.left + parseInt(centers[0]) - window.pageXOffset

        radians = Math.atan2(e.clientX - centerX, e.clientY - centerY)
        degree = (radians * (180 / Math.PI) * -1) + 180
        span.id = `${i}`



        // if ((centerX - e.clientX < 50 && centerX - e.clientX > -50) && (centerY - e.clientY <
        //         50 && centerY - e.clientY > -50)) {
        // console.log("span", span.id)
        // console.log("e.clientX", centerX - e.clientX)
        // console.log("e.clientY", centerY - e.clientY)

        span.style.transform = "rotate(" + degree + "deg)"
        // span.style.padding = "4px"
        // span.style.top = `${-12}px`
        // } else {
        //     span.style.transform = "rotate(0deg)"
        //     //  span.style.padding = "0px"
        // }
        // console.log(span)
    })
})
