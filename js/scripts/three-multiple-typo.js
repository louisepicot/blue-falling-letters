
var canvas;
const scenes = [],
    yesNo = true,
    containers = document.querySelectorAll(".three-container"),
    loader = new THREE.FontLoader();

const material = new THREE.MeshPhongMaterial({
    color: 0x2500ff,
    specular: 0x00ffc1,
    shininess: 100,
    flatShading: THREE.FlatShading,
    side: THREE.DoubleSide
});




window.onload = init()
animate()


function init() {


    for (let i = 0; i < 5; i++) {

        console.log(containers[i])
        let scene = new THREE.Scene(),
            renderer = window.WebGLRenderingContext ? new THREE.WebGLRenderer({ alpha: true }) : new THREE.CanvasRenderer(),
            light = new THREE.AmbientLight(0xffffff),
            camera,
            controls;
        containers[i].appendChild(renderer.domElement);

        scene.userData.renderer = renderer
        scene.add(light);

        const lightDir = new THREE.DirectionalLight(0x00ffc1);
        lightDir.position.set(0, 5, 100);
        lightDir.castShadow = true;
        scene.add(lightDir)
        const lightDirBack = new THREE.DirectionalLight(0x2500ff);
        lightDirBack.position.set(0, 5, -200);
        lightDirBack.castShadow = true;
        scene.add(lightDirBack)

        const lightDirRight = new THREE.DirectionalLight(0xc3b5ff);
        lightDirRight.position.set(-50, 25, 0);
        lightDirRight.castShadow = true;
        scene.add(lightDirRight)

        const lightDirLeft = new THREE.DirectionalLight(0x2500ff);
        lightDirLeft.position.set(50, -25, 0);
        lightDirLeft.castShadow = true;
        scene.add(lightDirLeft)


        camera = new THREE.PerspectiveCamera(30, 600 / 600, 1, 1000);
        camera.position.set(0, 0, 900);
        camera.lookAt(scene.position)
        scene.userData.camera = camera
        scene.add(camera)


        controls = new THREE.OrbitControls(scene.userData.camera)
        controls.dispose();
        controls.update();
        scene.userData.controls = controls
        containers[i].addEventListener('mousemove', (event) => { onDocumentMouseMove(event, controls) }, false);


        renderer.setSize(600, 600);
        renderer.setClearColor(0xffffff, 0);
        scenes.push(scene);
        loadFont(scene)
    }

    console.log(scenes)


}



function onDocumentMouseMove(event, controls) {
    controls.handleMouseMoveRotate(event)
    // controls.handleMouseMoveRotate(event);

}

function animate() {
    render()
    requestAnimationFrame(animate);
}


function render() {
    scenes.forEach(scene => {
        scene.userData.controls.update();
        let camera = scene.userData.camera
        scene.userData.renderer.render(scene, camera)
    })
}

function loadFont(scene) {
 
    loader.load('./assets/fonts/H2C_Regular.json', function (res) {
        font = res;
        createText(scene);
    });
}
function createText(scene) {
    let textgeo = new THREE.TextGeometry('TECHNOFE', {
        font: font,
        size: 70,
        height: 10,
        curveSegments: 12,
        weight: "bold",
        bevelThickness: 1,
        bevelSize: 1,
        bevelEnabled: true
    })
    textgeo.computeBoundingBox();
    textgeo.computeVertexNormals();

    let text = new THREE.Mesh(textgeo, material)
    text.position.x = -textgeo.boundingBox.max.x / 2;
    text.castShadow = true;
    scene.add(text)
}



// window.onload = init();